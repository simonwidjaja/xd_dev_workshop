# xdpm
https://github.com/AdobeXD/xdpm

(presentation by Pablo Klaschka)

---
## What it is
> "`xdpm` is a command line tool that makes it easy to develop Adobe XD plugins."
> 
> (project's README)


## Without xdpm
<img src="https://i.ibb.co/z669HKb/image.png" alt="image" border="0">

## With xdpm

<img src="https://i.ibb.co/s544CJm/image.png" alt="image" border="0">


## Installation
### Requirements
- [Node.js](https://nodejs.org/en/)
- npm (shipped with Node)

### Installing

```shell
$ npm install -g @adobe/xdpm
```

## Commands

### `xdpm install`
Installs the plugin in the current directory into the `develop` folder

### `xdpm watch`
Runs `xdpm install`, then watches the directory for changes and re-runs `xdpm
install` whenever something changes

### `xdpm package`
Validates and packages the plugin in the current directory into a `.xdx` that's
ready for distribution

### (more)
check out the [project's repository](https://github.com/AdobeXD/xdpm) for more info...
