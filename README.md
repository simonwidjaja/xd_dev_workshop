# XD_Dev_workshop

WELCOME TO THE ADOBE XD PLUGIN WORKSHOP & HACKATHON

# Agenda

![Agenda](Agenda.png "Agenda")

# Download
Please download your workshop assets from here:
https://gitlab.com/simonwidjaja/xd_dev_workshop/-/raw/master/download/FirstPlugin_Files.zip

# Overview

## First Plugin
What we will cover:
+ Environment, setup and requirements
+ Plugin UI (Panel)
+ Basic Debugging
+ Working with selection object
+ Edit Context
+ Element Creation


## Additional Challenges
+ Input field for gap between entries
+ Different colors (random or given array)



#
#
# 
# Step by step
 

# 1. Creating a new local plugin in devleopment mode

1. Locate the `/develop` folder by clicking `Adobe XD ▶ Plugins ▶ Development ▶ Show Develop folder`
1. Create new plugin folder with the name `FirstPlugin`
1. Locate provided workshop files on your disk
1. Copy everything from `steps/010_Boilerplate` to the previously created folder `FirstPlugin`
1. Now you have successfully  created and activated a blank plugin in XD. To see your plugin in action open the plugin panel in XD.
    + To reload all plugins press `SHIFT + CTRL/CMD + R` or call `Adobe XD ▶ Plugins ▶ Development ▶ Reload Plugins`
1. Open a code editor of your choice and open the `FirstPlugin` folder created earlier
1. Inspect `manifest.json`

# 2. Understanding the boilerplate and the basic plugin lifecycle
1. Open the main plugin file `main.js`, inspect the boilerplate code and locate the `console.log()` calls
1. Open XD's built in developer console by calling `Adobe XD ▶ Plugins ▶ Development ▶ Developer Console`
1. Reload plugins again (`SHIFT + CTRL/CMD + R`) and watch the developer console for output

# 3. Creating the basic UI
1. Add the UI markup by replace the following line ``const HTML = `Boilerplate`;`` with
    ```javascript
        const HTML =
          `<style>
            .break {
                flex-wrap: wrap;
            }
            label.row > span {
                color: #8E8E8E;
                width: 20px;
                text-align: right;
                font-size: 9px;
            }
            label.row input {
                flex: 1 1 auto;
            }
            .show {
                display: block;
            }
            .hide {
                display: none;
            }
          </style>
          <form method="dialog" id="main">
            <div class="row break">
              <div id="warning">Please select a single artboard first...</div>
              <div id="controls">
                <!--
                <label class="row">
                  <span>Count</span>
                  <input type="number" uxp-quiet="true" id="input-count" value="3" />
                </label>
                -->
                <button id="ok" type="submit" uxp-variant="cta">Run</button>
              </div>
            </div>
          </form>
        `;
    ```
1. Reload plugin in XD and inspect the created UI. It doesn't do anything yet, but you should already see the message and the button.
1. Hide both the message and the controls by addind the `hide` class to both div containers:
    ```html
      <div id="warning" class="hide">Please select a single artboard first...</div>
      <div id="controls" class="hide">
    ```
1. Import `selection` and `Artboard` from the XD API by adding the following line to the top 
    ```javascript
      const {selection, Artboard} = require("scenegraph");
    ```
1. Now we want to add context sensivity to our UI. Our plugin only works if the user selects an artboard. If no artboard is selected our plugin shows a warning. We can use the `update()` function to listen for selection changes. Replace the empty `update()`function with the following code:
    ```javascript
    function update() {
      console.log(LOG, 'update()');
      // Only process if an Artboard is selected
      const valid = selection.items[0] instanceof Artboard;
      panel.querySelector('#warning').className = valid ? 'hide' : 'show';
      panel.querySelector('#controls').className = valid ? 'show' : 'hide';
    }    
    ```
# 4. Implementing the feature
1. Add the following `run()` function right after the `update()` function:
    ```javascript
    function run() {
      console.log(LOG, 'run()');
    }
    ```
1. Add an event listener to the UI button to call `run()` by adding the following line to the `createPanel()`function:
    ```javascript
    panel.querySelector("form").addEventListener("submit", run);
    ```
1. Test button behaviour in XD to see the added log in the console.
1. Import `Rectangle` and `Color` by adding them to the require statement in the fist line:
    ```javascript
    const {selection, Artboard, Rectangle, Color} = require("scenegraph");
    ```
1. Let's try to create our first element by adding the following line to `run()`:
    ```javascript
    // Create Rectangle
    let rect1 = new Rectangle();
    rect1.fill = new Color('#BADA55');
    rect1.width = 80;
    rect1.height = 80;
    selection.insertionParent.addChild(rect1);
    ```
1. Test in XD and inspect developer console. The following message appears:
    ```
    Plugin Error: Plugin FirstPlugin is not permitted to make changes from the background. Use editDocument() for panel UI handlers, or return a Promise to extend an edit operation asynchronously.
    ```
    This is due to the edit context constraints. We cannot alter the document screnegraph without starting an `editDocument` session first (this only applies for panels not modal dialogs). So let's start a session first...
1. Import editDocument by adding the following line to the head:
    ```javascript
    const {editDocument} = require("application");
    ```
1. Wrap the rectangle creation part in a `editDocument` session:
    ```javascript
      editDocument({ editLabel: "Generate elements" }, function (selection) {

        // Create Rectangle
        let rect1 = new Rectangle();
        rect1.fill = new Color('#BADA55');
        rect1.width = 80;
        rect1.height = 80;
        selection.insertionParent.addChild(rect1);

      });    
    ```
1. Test in XD again to see the programmatically created rectangle.
1. Create 2 more rectangles to create a teaser like composition:
    ```javascript
      //... first Rectangle

      // Create Rectangle
      let rect2 = new Rectangle();
      rect2.fill = new Color('#DDD');
      rect2.width = 200;
      rect2.height = 10;
      rect2.moveInParentCoordinates(100, 15);
      selection.insertionParent.addChild(rect2);

      // Create Rectangle
      let rect3 = new Rectangle();
      rect3.fill = new Color('#EEE');
      rect3.width = 200;
      rect3.height = 30;
      rect3.moveInParentCoordinates(100, 35);
      selection.insertionParent.addChild(rect3);
    ```
1. Since our plugin will create more and more elements everytime we click the button, our document gets polluted quickly. Let's empty the selected artboard first everytime the user clicks the button (this is only for development!!!). Just add the following line before creating the first rectangle:
    ```javascript
      // (Only for development) Remove all elements in artboard
      selection.insertionParent.removeAllChildren();
    ```

# 5. (Optional) Let the user define how many new teasers to be created via input field
1. Uncomment the input markup in the HTML constant defined in `createPanel()`
1. Modify the creation part of our 3 rectangles:
    ```javascript
      let count = Number(document.querySelector("#input-count").value);
      let height = 100;

      for (var i = 0; i < count; i++) {
        // Create Rectangle
        let rect1 = new Rectangle();
        rect1.fill = new Color('#BADA55');
        rect1.width = 80;
        rect1.height = 80;
        rect1.moveInParentCoordinates(0, i * height);
        selection.insertionParent.addChild(rect1);
    
        // Create Rectangle
        let rect2 = new Rectangle();
        rect2.fill = new Color('#DDD');
        rect2.width = 200;
        rect2.height = 10;
        rect2.moveInParentCoordinates(100, i * height + 15);
        selection.insertionParent.addChild(rect2);
    
        // Create Rectangle
        let rect3 = new Rectangle();
        rect3.fill = new Color('#EEE');
        rect3.width = 200;
        rect3.height = 30;
        rect3.moveInParentCoordinates(100, i * height + 35);
        selection.insertionParent.addChild(rect3);
      }
    ```
1. Voilá! You now have created a XD plugin (almost) from scratch featuring a custom UI, context sensitivity, element creation and user input. 