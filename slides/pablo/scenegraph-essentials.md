# Scenegraph Essentials
(Pablo Klaschka)


## A little bit of CS: Tree structures
<img src="https://i.ibb.co/9p5HtFG/image.png" alt="image" border="0">

## Applying it to XD

- Scenegraph is a tree structure that represents the document
- Layer = Node
- Nodes (usually) have properties like position, rotation, scale
- Nodes can have different types, e.g., `Artboard`, `Rectangle`, etc.
- As seen: These types are *requirable* **classes** from `require('scenegraph')`
- root node of the scenegraph is always a `RootNode`

### Types of nodes and terminology
- *can't be leaf nodes*: `RepeatGrid`, `Group`, `BooleanGroup`
- *can be leaf nodes*: `Artboard` (can exist with and without children)
- *must be leaf nodes*: `Text`, `Rectangle`, `Polygon`, etc.

**Documentation and forums use *leaf node* synonymously for *must-be leaf nodes*!**

## One example

|scenegraph| XD Document|
|--|--|
| <img src="https://i.ibb.co/whzstxc/image.png" alt="image" border="0"> | (intentionally blank) |

---

|scenegraph| XD Document|
|--|--|
| <img src="https://i.ibb.co/6DWZN16/image.png" alt="image" border="0"> | <img src="https://i.ibb.co/FBVS3x5/image.png" alt="image" border="0"> |

---

|scenegraph| XD Document|
|--|--|
| <img src="https://i.ibb.co/G9dGkL6/image.png" alt="image" border="0"> | <img src="https://i.ibb.co/PFwmJj8/image.png" alt="image" border="0">
 |

---

|scenegraph| XD Document|
|--|--|
| <img src="https://i.ibb.co/xsjqdPL/image.png" alt="image" border="0"> | <img src="https://i.ibb.co/7J491WL/image.png" alt="image" border="0">
 |

---

|scenegraph| XD Document|
|--|--|
| <img src="https://i.ibb.co/v3VXmr7/image.png" alt="image" border="0"> | <img src="https://i.ibb.co/6R3fvgC/image.png" alt="image" border="0">
 |
  
