### Adobe XD Developer Workshop
# Shortcuts
+ Reload plugins: `SHIFT + CTRL/CMD + R`
+ Developer console: `Adobe XD ▶ Plugins ▶ Development ▶ Developer Console`

# Code Snippets

## Get references to XD API
    // Scenegraph
    const scenegraph = require("scenegraph");
    // Sub classes
    const { Line, Rectangle, Color } = require("scenegraph");
    // Commands
    const commands = require("commands");
    // Edit Context
    const {editDocument} = require("application");

## Creating elements

Creating a rectangle
 
    const rect = new Rectangle();
    rect.width = 100;
    rect.height = 50;
    rect.fill = new Color('#BADA55');

    selection.insertionParent.addChild(rect);
    rect.moveInParentCoordinates(100, 100);

## Useful during development

Deleting all elements in parent

    // Be careful with this one ;)
    selection.insertionParent.removeAllChildren();
