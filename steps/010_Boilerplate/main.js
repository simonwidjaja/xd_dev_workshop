const LOG = '[ FirstPlugin ]';
let panel;

function createPanel() {
  const HTML = `Boilerplate`;
  panel = document.createElement("div");
  panel.innerHTML = HTML;
  return panel;
}

function show(event) {
  console.log(LOG, 'show()');
  if (!panel) event.node.appendChild( createPanel() );
}

function update() {
  console.log(LOG, 'update()');
}

module.exports = {
  panels: {
    myPanel: { show, update }
  }
};
