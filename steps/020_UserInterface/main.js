const {selection, Artboard} = require("scenegraph");

const LOG = '[ FirstPlugin ]';
let panel;

function createPanel() {
  const HTML =
    `<style>
      .break {
          flex-wrap: wrap;
      }
      label.row > span {
          color: #8E8E8E;
          width: 20px;
          text-align: right;
          font-size: 9px;
      }
      label.row input {
          flex: 1 1 auto;
      }
      .show {
          display: block;
      }
      .hide {
          display: none;
      }
    </style>
    <form method="dialog" id="main">
      <div class="row break">
        <div id="warning" class="hide">Please select a single artboard first...</div>
        <div id="controls" class="hide">
          <!--
          <label class="row">
            <span>Count</span>
            <input type="number" uxp-quiet="true" id="input-count" value="3" />
          </label>
          -->
          <button id="ok" type="submit" uxp-variant="cta">Run</button>
        </div>
      </div>
    </form>
  `;
  panel = document.createElement("div");
  panel.innerHTML = HTML;
  return panel;
}

function show(event) {
  console.log(LOG, 'show()');
  if (!panel) event.node.appendChild(createPanel());
}

function update() {
  console.log(LOG, 'update()');
  // Only process if an Artboard is selected
  const valid = selection.items[0] instanceof Artboard;
  panel.querySelector('#warning').className = valid ? 'hide' : 'show';
  panel.querySelector('#controls').className = valid ? 'show' : 'hide';
}

module.exports = {
  panels: {
    myPanel: { show, update }
  }
};
