const {selection, Artboard, Ellipse, Rectangle, Color} = require("scenegraph");
const {editDocument} = require("application");
const LOG = '[ FirstPlugin ]';
let panel;

function createPanel() {
  const HTML =
    `<style>
      .break {
          flex-wrap: wrap;
      }
      label.row > span {
          color: #8E8E8E;
          width: 20px;
          text-align: right;
          font-size: 9px;
      }
      label.row input {
          flex: 1 1 auto;
      }
      .show {
          display: block;
      }
      .hide {
          display: none;
      }
    </style>
    <form method="dialog" id="main">
      <div class="row break">
        <div id="warning" class="hide">Please select a single artboard first...</div>
        <div id="controls" class="hide">
          <label class="row">
            <span>Count</span>
            <input type="number" uxp-quiet="true" id="input-count" value="3" />
          </label>
          <button id="ok" type="submit" uxp-variant="cta">Run</button>
        </div>
      </div>
    </form>
  `;

  panel = document.createElement("div");
  panel.innerHTML = HTML;
  panel.querySelector("form").addEventListener("submit", run);

  return panel;
}

function show(event) {
  if (!panel) event.node.appendChild( createPanel() );
}

function update() {
  console.log(LOG, 'update()');

  // Only process if an Artboard is selected
  const valid = selection.items[0] instanceof Artboard;
  panel.querySelector('#warning').className = valid ? 'hide' : 'show';
  panel.querySelector('#controls').className = valid ? 'show' : 'hide';
}

function run() {
  console.log(LOG, 'run()');

  editDocument({ editLabel: "Generate elements" }, function (selection) {

    // (Only for development) Remove all elements in artboard
    selection.insertionParent.removeAllChildren();

    let count = Number(document.querySelector("#input-count").value);
    let height = 100;

    for (var i = 0; i < count; i++) {

      // Create Rectangle
      let rect1 = new Rectangle();
      rect1.fill = new Color('#BADA55');
      rect1.width = 80;
      rect1.height = 80;
      rect1.setAllCornerRadii(Math.random()*45);
      rect1.moveInParentCoordinates(0, i*height);
      selection.insertionParent.addChild(rect1);

      // Create Rectangle
      let rect2 = new Rectangle();
      rect2.fill = new Color('#DDD');
      rect2.width = 200;
      rect2.height = 10;
      rect2.moveInParentCoordinates(100, i*height + 15);
      selection.insertionParent.addChild(rect2);

      // Create Rectangle
      let rect3 = new Rectangle();
      rect3.fill = new Color('#EEE');
      rect3.width = 200;
      rect3.height = 30;
      rect3.moveInParentCoordinates(100, i*height + 35);
      selection.insertionParent.addChild(rect3);

    }

  });

}

module.exports = {
  panels: {
    myPanel: { show, update }
  }
};
